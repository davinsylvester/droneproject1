package com.techforce1.drone;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ImageFetchService {

    private byte[] latestImage;

    public ImageFetchService() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::fetchImage, 0, 2, TimeUnit.SECONDS);
    }

    public void fetchImage() {
        RestTemplate restTemplate = new RestTemplate();
        byte[] imageBytes = restTemplate.getForObject("http://localhost:8081/api/drone/video/frame", byte[].class);
        this.latestImage = imageBytes;
    }

    public byte[] getLatestImage() {
        return latestImage;
    }
}
