package com.techforce1.drone.controllers;

import com.techforce1.drone.ImageFetchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;

@Controller
public class DroneController {

    @Autowired
    private ImageFetchService imageFetchService;

    @GetMapping("/drone/video/frame/view")
    public String displayDroneFrame() {
        return "droneImage";
    }

    @GetMapping("/drone/video/frame/latest")
    @ResponseBody
    public ResponseEntity<byte[]> getLatestFrame() {
        byte[] image = imageFetchService.getLatestImage();
        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
    }
}
